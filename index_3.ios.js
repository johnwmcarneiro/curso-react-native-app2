import React from 'react';
import { AppRegistry, View, Button } from 'react-native';

const Estilos = {
  principal: {
    paddingTop: 40
  }
};

const botaoPressionado = () => {
  alert('Botão pressionado');
}

const App = () => {
  const { principal } = Estilos;
  return (
    <View style={ principal }>
      <Button
        onPress={botaoPressionado}
        title="Clique aqui"
        color="#841584"
        accessibilityLabel="Clique para abrir as notícias!"
      />
    </View>
  );
};

AppRegistry.registerComponent('app2', () => App);