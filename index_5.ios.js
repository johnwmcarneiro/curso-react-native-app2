import React from 'react';
import { Text, AppRegistry, View, Image } from 'react-native';

const Estilos = {
  principal: {
    paddingTop: 40
  },
  imagem: {
    justifyContent: 'flex-end',
    padding: 5
  }
};

const App = () => {
  const { principal, imagem } = Estilos;
  return (
    <View style={principal}>
      <Image
        source={require('./imgs/uvas.png')}
        style={imagem}
      >
        <Text>Legenda para a foto</Text>
      </Image>

      <Image
        source={{ uri: 'https://reactjs.org/logo-og.png' }}
        style={{ width: 400, height: 400 }}
      />
    </View>
  );
};

AppRegistry.registerComponent('app2', () => App);
